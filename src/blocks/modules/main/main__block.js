import Swiper from "swiper";

var sliderBigTop = new Swiper('.slider-big-top', {
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    pagination: {
        el: '.swiper-pagination--top',
        clickable: true,
    },
});
