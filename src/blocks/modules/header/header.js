import $ from 'jquery'


$(".dropdown-menu__item").hover(function () {
    var menuHeight = $(this).find('.dropdown-submenu__wrap').height()
    $(this).addClass('active');
    $(this).siblings().removeClass('active');
    $('.dropdown-menu__wrap').height(menuHeight+ 100).css('transition-duration', '0.5s')

});

$(".dropdown-submenu__item").hover(function () {
    $(this).addClass('active');
    $(this).siblings().removeClass('active');
});

$(".dropdown-equipment__item").hover(function () {
    var parent = $(this).parents('.dropdown-equipment__item--wrap')
    parent.addClass('active');
    parent.siblings().removeClass('active');
});



$('.header__menu--link').on('click', function() {
    var parent = $(this).parents('.header__menu__item--wrap')
    parent.toggleClass('active');
    parent.siblings().removeClass('active');
    // $('.nano').nanoScroller({alwaysVisible: true});
    var menuHeight = $(this).parents('.header__menu--item').find('.dropdown-submenu__wrap').height()
    $('.dropdown-menu__wrap').height(menuHeight+ 100)

});

$('.close-menu').on('click', function() {
    var parent = $(this).parents('.header__menu__item--wrap');
    parent.removeClass('active');

});
