import Swiper from "swiper";

var sliderLogo = new Swiper('.slider-logo', {
    slidesPerView: 4,
    slidesPerColumn: 2,
    slidesPerColumnFill: 'row',
    slidesPerGroup: 1,
    spaceBetween: 30,
    preloadImages: false,
    lazy: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});
