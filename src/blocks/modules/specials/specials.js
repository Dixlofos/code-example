import '../../../js/import/vendor/anitabs.min'
import '../../../js/import/vendor/slick'
import '../../../js/import/vendor/jquery.nanoscroller.min'


$('.js-tabs-link').aniTabs({
    animation: 'fade',
    slideDirection: 'left', // or right
    animationSpeed: 500,// default: 500
});


$.each($('.slider-product'), function() {
    $(this).slick({
        slidesToScroll: 1,
        slidesToShow: 6,
        arrows: true,
        dots: false,
        lazyLoad: 'ondemand',
        infinite: false,
        prevArrow: '<a href="#" class="prev-button"></a>',
        nextArrow: '<a href="#" class="next-button"></a>',
        appendArrows: $(this).parents('.slider-slick__wrap').find('.container-arrows'),
    });

    $('.slider-big__item').hover(function() {
        var first = $('.slick-active:first').data('slick-index');
        var last = $('.slick-active:last').data('slick-index');
        if ($(this).data('slick-index') == first) {
            $(this).addClass('slick-item-left');
        }
        if ($(this).data('slick-index') == last) {
            $(this).addClass('slick-item-right');
        }
    });

});
