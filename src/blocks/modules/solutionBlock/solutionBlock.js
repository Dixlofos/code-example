import Swiper from "swiper";

var sliderSolution = new Swiper('.slider-solution', {
    slidesPerView: 2,
    spaceBetween: 15,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

});
